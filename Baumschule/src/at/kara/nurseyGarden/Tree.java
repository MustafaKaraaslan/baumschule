package at.kara.nurseyGarden;

public class Tree {
	
 private int maxDiameter;
 private int maxHight;
 private fertilize usedMethod;
 
public Tree(int maxDiameter, int maxHight, fertilize usedMethod) {
	super();
	this.maxDiameter = maxDiameter;
	this.maxHight = maxHight;
	this.usedMethod = usedMethod;
}


public int getMaxDiameter() {
	return maxDiameter;
}
public void setMaxDiameter(int maxDiameter) {
	this.maxDiameter = maxDiameter;
}
public int getMaxHight() {
	return maxHight;
}
public void setMaxHight(int maxHight) {
	this.maxHight = maxHight;
}
public fertilize getUsedMethod() {
	return usedMethod;
}
public void setUsedMethod(fertilize usedMethod) {
	this.usedMethod = usedMethod;
}
 
 
}
