package at.kara.nurseyGarden;

import java.util.List;

public class Area {
	private String Aname;
	private int Asize;
	private List<Tree> treeList;
	
	
	public Area(String aname, int asize, List<Tree> treeList) {
		super();
		Aname = aname;
		Asize = asize;
		this.treeList = treeList;
	}


	public String getAname() {
		return Aname;
	}


	public void setAname(String aname) {
		Aname = aname;
	}


	public int getAsize() {
		return Asize;
	}


	public void setAsize(int asize) {
		Asize = asize;
	}


	public List<Tree> getTreeList() {
		return treeList;
	}


	public void setTreeList(List<Tree> treeList) {
		this.treeList = treeList;
	}
	
	
	
	
}
