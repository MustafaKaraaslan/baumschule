package at.karaaslan.car.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import at.karaaslan.cars.Car;
import at.karaaslan.cars.Producer;
import junit.framework.Assert;

class CatTest {

	@Test
	  void testGetPriceWithDiscount() {
	    Producer p1 = new Producer(10,"Tesla");
	    Car c1 = new Car(p1,190000,"ModelS");
	    Assert.assertEquals(171000, c1.getPriceWithDiscount());
	  }
	  
	  @Test
	  void testGetPriceWithDiscount1() {
	    Producer f1 = new Producer(10,"Tesla");
	    Car c2 = new Car(f1,120000,"ModelX");
	    Assert.assertEquals(108000, c2.getPriceWithDiscount());
	  }
	  @Test
	  void testGetPriceWithDiscount2() {
	    Producer v1 = new Producer(0,"Tesla");
	    Car c3 = new Car(v1,25000,"Model3");
	    Assert.assertEquals(25000, c3.getPriceWithDiscount());
	  }

}
