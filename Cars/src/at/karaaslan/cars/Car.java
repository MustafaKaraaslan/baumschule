package at.karaaslan.cars;

public class Car {
	private String id;
	private Producer producer;
	private int price;
	

	public Car(Producer producer, int price, String id) {
		super();
		this.producer = producer;
		this.id = id;
		this.price = price;
		
		
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public Producer getProducer() {
		return producer;
	}


	public void setProducer(Producer producer) {
		this.producer = producer;
	}


	public int getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}
	
	
	public int getPriceWithDiscount() {
		double discountInDecimal = this.producer.getDiscount() * 0.01;
		double multiplyFactor = 1.0 - discountInDecimal;
		
		double result;
		if(multiplyFactor > 0) {
			result = this.price * multiplyFactor;
		} else {
			result = this.price;
		}
		
		return (int) result;
	}
	

}
