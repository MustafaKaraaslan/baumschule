
public class Producer {
	public int discount;
	public String name;
	
	public Producer(int discount, String name) {
		super();
		this.discount = discount;
		this.name = name;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
}
